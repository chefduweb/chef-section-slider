<?php

	namespace ChefSectionSlider\Front;

	use Cuisine\Wrappers\Field;
	use ChefSectionSlider\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;


	class PanelListener extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->listen();

		}

		/**
		 * Listen for SettingsPanels
		 * 
		 * @return void
		 */
		private function listen(){


			add_action( 'init', function(){

				$fields = $this->getSectionSettingFields();

				Panel::make( 
					__( 'Slider settings', 'chefsectionslider' ),
					'slider-settings',
					array(
						'position' 		=> 10,
						'icon'			=> 'dashicons-editor-expand',
						'rules' 		=> [ 'inContainer' => 'slider' ]
					)

				)->set( $fields );

			});
		}

		/**
		 * Returns an array of field objects
		 * 
		 * @return array
		 */
		private function getSectionSettingFields(){
			
			$fields = array(

				Field::image( 
					'sectionBg',
					__( 'Background', 'chefsectiontabs' )
				)

			);

			return $fields;
		}


	}

	if( !is_admin() )
		\ChefSectionSlider\Front\PanelListener::getInstance();
