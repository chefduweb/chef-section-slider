<?php

	namespace ChefSectionSlider\Listeners;

	use \Cuisine\Utilities\Url;
	use \Cuisine\Wrappers\Route;
	use \Cuisine\Wrappers\PostType;
	use \ChefSectionSlider\Wrappers\StaticInstance;

	class EventListeners extends StaticInstance{


		/**
		 * Init events & vars
		 */
		function __construct(){

			$this->listen();

		}


		/**
		 * Listen to front-end events
		 *
		 * @return void
		 */
		private function listen(){

			/**
			 * Add the container
			 */
			add_filter( 'chef_sections_containers', function( $data ){

				$template = Url::path( 'plugin', 'chef-section-slider/Assets/template.php', false );

				$data[ 'slider' ] = [
					'label' 	=> 'Section Slider',
					'view' 		=> 'tabbed',
					'template' 	=> $template,
					'class' 	=> '\ChefSectionSlider\Hooks\SliderContainerSection'
				];
				
				return $data;
			});


			/**
			 * Set Section Classes
			 */
			add_filter( 'chef_section_classes', function( $class, $section ){

				if( !is_null( $section->container_id ) ){

					$collection = new InContainerCollection( $section->post_id, $section->container_id );
					$first = $collection->toArray()->first();

					$class .= ' slider-content-'.$section->id;

					if( $first['id'] == $section->id )
						$class .= ' active';

				}

				return $class;

			}, 100, 2);

		}



	}

	\ChefSectionSlider\Listeners\EventListeners::getInstance();
