<?php

    namespace ChefSectionSlider\Wrappers;
    
    class StaticInstance {
    
        /**
         * Static bootstrapped instance.
         *
         * @var \ChefSectionSlider\Wrappers\StaticInstance
         */
        public static $instance = null;
    
    
    
        /**
         * Init the Assets Class
         *
         * @return \ChefSectionSlider\Admin\Assets
         */
        public static function getInstance(){
    
            return static::$instance = new static();
    
        }
    
    
    } 